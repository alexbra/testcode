package com.alexbra.badoocodetest_alex_bra.Manager;

import android.content.Context;

import com.alexbra.badoocodetest_alex_bra.Model.TransactionDetail;
import com.alexbra.badoocodetest_alex_bra.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.alexbra.badoocodetest_alex_bra.Model.Transactions;

/**
 * Created by alex on 12/28/15.
 */
public class DataManager {
    //TODO convert from static to singleton

    public static String getJSONStringFromRaw(Context pContext, int id) {
        InputStream is = pContext.getResources().openRawResource(id);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];


        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (IOException e) {
            //TODO Handle the error... but the streams are still open!
        } finally {
            //close input
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ioex) {
                    //TODO Very bad things just happened... handle it
                }
            }
            //Close output
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ioex) {
                    //TODO Very bad things just happened... handle it
                }
            }
        }

        String jsonString = writer.toString();
        return jsonString;
    }

    public static JSONArray getJSONArrayFromRaw(Context pContext, int id) {
        String jsonString = getJSONStringFromRaw(pContext, id);
        JSONArray pJSONArray = null;
        try {
            pJSONArray = new JSONArray(jsonString);
        } catch (JSONException e) {
            //TODO
            e.printStackTrace();
        }
        return pJSONArray;
    }

    //TODO MV ABOVE to utils class

    public static JSONArray getTransactions(Context pContext) {
        return getJSONArrayFromRaw(pContext, R.raw.transactions);
    }

    public static JSONArray getRates(Context pContext) {
        return getJSONArrayFromRaw(pContext, R.raw.rates);
    }


    private static final String SKU = "sku";
    private static Map<String, Integer> transactionsMap;
    private static Map<String, List<TransactionDetail>> transactionDetailMap;
    private static List<Transactions> transactionsList;

    public static Map<String, List<TransactionDetail>> getTransactionDetailMap(Context pContext) {
        if(transactionDetailMap == null) {
            try {
                getTransactionsMap(pContext);
            } catch (JSONException e) {
                e.printStackTrace();
                //TODO
            }
        }
        return transactionDetailMap;
    }

    public static Map<String, Integer> getTransactionsMap(Context pContext) throws JSONException {
        //Cache :)
        if(transactionsMap != null && transactionDetailMap != null)
            return transactionsMap;

        Map<String, Integer> answerMap = new TreeMap<String, Integer>();
        //TODO not good transactionDetailMap and transactionsMap can be merged
        transactionDetailMap = new TreeMap<String, List<TransactionDetail>>();
        JSONArray pJSONArray =  getTransactions(pContext);
        for(int i = 0; i < pJSONArray.length(); i++){
            JSONObject tmpJSONObject = pJSONArray.getJSONObject(i);
            String sku = tmpJSONObject.getString(SKU);

            Integer num = answerMap.get(sku);
            if(num == null)
                num = 0;

            num++;
            answerMap.put(sku,num);

            // -- transactionDetailMap --

            List<TransactionDetail> tmpList = transactionDetailMap.get(sku);
            if(tmpList == null)
                tmpList = new ArrayList<TransactionDetail>();
            tmpList.add(new TransactionDetail(sku, tmpJSONObject));
            transactionDetailMap.put(sku,tmpList);
        }

        transactionsMap = answerMap;
        return transactionsMap;
    }

    public static List<Transactions> getTransactionsList(Context pContext) {
        //Cache :)
        if(transactionsList != null)
            return transactionsList;

        List<Transactions> answerList = new ArrayList<>();

        Map<String, Integer> tmap = null;
        try {
            tmap = getTransactionsMap(pContext);
        } catch (JSONException e) {
            //TODO
            e.printStackTrace();
            return answerList;
        }
        Set set = tmap.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            answerList.add(new Transactions((String)mentry.getKey(), (Integer)mentry.getValue()));
        }

        transactionsList = answerList;
        return transactionsList;
    }
}
