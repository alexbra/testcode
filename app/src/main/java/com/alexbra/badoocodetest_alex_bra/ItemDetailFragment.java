package com.alexbra.badoocodetest_alex_bra;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.alexbra.badoocodetest_alex_bra.Manager.DataManager;
import com.alexbra.badoocodetest_alex_bra.Model.TransactionDetail;
import com.alexbra.badoocodetest_alex_bra.Model.Transactions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends ListFragment {

    public static final String ARG_ITEM_ID = "ARG_ITEM_ID";


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {

            Utils.initExc(this.getActivity());

            Map<String, List<TransactionDetail>> transactionDetailMap = DataManager.getTransactionDetailMap(this.getActivity());
            String arg = getArguments().getString(ARG_ITEM_ID);

            this.getActivity().setTitle("Transactions for "+arg);


            //TODO better!!!!! use custom layout in adapter

            List<Map<String, String>> data = new ArrayList<Map<String, String>>();

            final List<TransactionDetail> tmpList = transactionDetailMap.get(arg);
            //TODO better do it all in cents
            double totalSUM = 0.;
            for (TransactionDetail transDetail : tmpList) {
                Map<String, String> datum = new HashMap<String, String>(2);

                double inGBP = transDetail.getInGBP();
                totalSUM += inGBP;

                datum.put("First Line", Utils.getFormattedNum(transDetail.amount,transDetail.currency));
                datum.put("Second Line", Utils.getFormattedNum(inGBP,"GBP"));
                data.add(datum);
            }
            SimpleAdapter adapter = new SimpleAdapter(this.getActivity(), data,
                    android.R.layout.simple_list_item_2,
                    new String[]{"First Line", "Second Line"},
                    new int[]{android.R.id.text1, android.R.id.text2});

            setListAdapter(adapter);

            TextView total = (TextView) getActivity().findViewById(R.id.total);
            if(total != null)
                total.setText("Total: "+Utils.getFormattedNum(totalSUM,"GBP"));
        } else {
            //TODO
        }
    }
}
