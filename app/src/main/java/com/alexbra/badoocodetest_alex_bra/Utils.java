package com.alexbra.badoocodetest_alex_bra;

import android.content.Context;

import com.alexbra.badoocodetest_alex_bra.Manager.DataManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * Created by alex on 12/28/15.
 */
public class Utils {
    //TODO maybe better to use enums than strings for currency

    static Map<String,String> currencyHash = new HashMap<String,String>();
    static{
        currencyHash.put("GBP","£");
        currencyHash.put("USD","$");
        currencyHash.put("CAD","CA$");
        currencyHash.put("AUD","A$");
        currencyHash.put("EUR","€");
    }


    public static String getFormattedNum(double number, String currency)  {

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator('\'');
        symbols.setDecimalSeparator(',');

        String sign = currencyHash.get(currency);
        if(sign == null)
            sign = currency;
        DecimalFormat decimalFormat = new DecimalFormat(sign+" #,##0.00", symbols);
        decimalFormat.setRoundingMode(RoundingMode.FLOOR);
        return decimalFormat.format(number);
    }

    //TODO better to mv to separate class

    private static Map<String,Map<String,Double>> exc;

    public static void initExc(Context pContext){
        JSONArray pJSONArray = DataManager.getRates(pContext);

        exc = new HashMap<String,Map<String,Double>>();

        for (int i = 0; i < pJSONArray.length(); i++){
            try {
                JSONObject tmp = pJSONArray.getJSONObject(i);
                String from = tmp.getString("from");
                String to = tmp.getString("to");
                double rate = tmp.getDouble("rate");
                Map<String,Double> tmpHash = exc.get(from);
                if(tmpHash == null) {
                    tmpHash = new HashMap<String, Double>();
                    exc.put(from,tmpHash);
                }
                tmpHash.put(to,rate);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private static Map<String,Map<String,Double>> getExc(){
        //TODO initExc must be called before! so better to mv to separ. class
        return exc;
    }

    public static String convertFromTo(double number, String from,String to)  {
        //TODO check for exceptions
        Double rate = getRateFromTo(from,to);
        if (rate != null){
            return number*rate+"";
        } else {
            return "Can not convert";
        }
    }
    public static String convertToGBP(double number, String from)  {
        //TODO check for exceptions
        if(from.equals("GBP")) {
            return number+"";
        }
        return convertFromTo(number,from, "GBP");
    }

    static Map<String,Boolean> visitedHash;
    public static Double getRateFromTo(String from,String to)  {
        //TODO check for exceptions
        Map<String,Double> tmp = getExc().get(from);
        if(tmp != null){
            Double rate = tmp.get(to);
            if(rate != null){
                visitedHash = null;
                return rate;
            } else {
                if(visitedHash == null)
                    visitedHash = new HashMap<String,Boolean>();
                //TODO better to use function
                visitedHash.put(from+"-"+to,true);
                //some kind of BFS
                Iterator it = tmp.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();
                    String tmpCurency = (String)pair.getKey();
                    Double tmpRate = (Double)pair.getValue();

                    //check if we already have visited it
                    if(visitedHash.get(tmpCurency+"-"+to) != null)
                        continue;
                    Double otherRate = getRateFromTo(tmpCurency,to);
                    if(otherRate != null)
                        return tmpRate*otherRate;
                }
            }
        }
        visitedHash = null;
        return null;
    }
}
