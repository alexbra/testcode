package com.alexbra.badoocodetest_alex_bra.Model;

/**
 * Created by alex on 12/28/15.
 * A item representing a piece of content.
 */
public class Transactions {
    public String sku;
    public Integer transactions;

    public Transactions(String sku, Integer transactions) {
        this.sku = sku;
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return sku+" "+transactions.toString();
    }
}
