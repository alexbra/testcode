package com.alexbra.badoocodetest_alex_bra.Model;

import com.alexbra.badoocodetest_alex_bra.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alex on 12/28/15.
 * A item representing a piece of content.
 */
public class TransactionDetail {
    public String sku;
    public JSONObject transaction;
    private Double inGBP;

    public double amount;
    public String currency;

    public TransactionDetail(String sku, JSONObject transaction) {
        this.sku = sku;
        this.transaction = transaction;

        try {
            //TODO to constant
            amount = transaction.getDouble("amount");
            currency = transaction.getString("currency");

        } catch (JSONException e) {
            e.printStackTrace();
            //TODO
        }
    }

    public Double getInGBP(){
        if(inGBP == null){
            String tmpInGBP = Utils.convertToGBP(amount, currency);
            if (tmpInGBP != null) {
                try {
                    inGBP = Double.parseDouble(tmpInGBP);
                } catch (NumberFormatException e){
                    e.toString();
                    //TODO
                    inGBP = 0.0;
                }
            } else {
                //TODO
                inGBP = 0.0;
            }
        }
        return inGBP;
    }
}
